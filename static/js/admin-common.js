//初始化md编辑器
var contextEditor;
$(function() {
    contextEditor = editormd("content", {
        width   : "100%",
        height  : 640,
        syncScrolling : "single",
        path    : "../../static/lib/editormd/lib/"
    });
});


$('.ui.dropdown').dropdown({
    on : 'click'
});


 // 数据校验
 $('.ui.form').form({
    fields : {
        title : {
            identifier : 'title',
            rules : [{
                type : 'empty',
                prompt : '标题：请输入博客标题'
            }]
        }
    }
});
