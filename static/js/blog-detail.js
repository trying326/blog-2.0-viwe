 //赞赏二维码弹出
 $('#payButtom').popup({
     popup: $('#payContent'),
     // on:'click',
     position: 'bottom center'
 });

 //生成目录
 tocbot.init({
     //目录生成后，装载目录的html元素
     tocSelector: '#toc-container',
     //依据那些内容生成目录，内容html容器
     contentSelector: '#content',
     //对应的目录级别的标签 (只对有id属性的h标签有效)
     headingSelector: 'h1, h2, h3 ,h4 ,h5'
 });

 //目录弹出
 $('.show-container').popup({
     popup: $('#toc-container'),
     on: 'click',
     position: 'left center'
 });

 //手机浏览二维码 生成
 var qrcode = new QRCode("qrcode", {
     text: window.location.href,
     width: 120,
     height: 120,
     colorDark: '#000000',
     colorLight: '#ffffff',
     correctLevel: QRCode.CorrectLevel.H
 });

 //手机浏览二维码 弹出
 $('.m-WeChat-code').popup({
     popup: $('#WeChat-code'),
     // on: 'click',
     position: 'left center'
 });

 //  回到顶部，平滑滚动
 $('#toTopButton').click(function() {
     // 0 : 回到顶部  500 : 滚动时间 毫秒
     $(window).scrollTo(0, 450);
 });

 //滚动侦测
 var waypoint = new Waypoint({
     element: document.getElementById('top'),
     handler: function(direction) {
         if (direction == 'down') {
             $('#toolbar').show(350);
         } else {
             $('#toolbar').hide(500);
         }
         console.log('Scrolled to waypoint!' + direction);
     }
 });